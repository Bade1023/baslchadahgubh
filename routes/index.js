const express = require('express')
const router = express.Router()
const { ensureAuthenticated } = require('../config/auth')

// Welcome Page
router.get('/', (req, res) => res.render('index'))
router.get('/about',(req,res)=> res.render('about'))
router.get('/contact',(req,res)=> res.render('contact'))
router.get('/post',(req,res)=> res.render('post'))
router.get('/login',(req,res)=> res.render('server'))


// Dashboard
router.get('/dashboard', ensureAuthenticated, (req, res) => res.render('dashboard', {
    name: req.user.name
}))

module.exports = router
 

